import math
from bisect import bisect_left, bisect_right
from compression.compressor import Compressor
from compression.compressor import get_size_of_files
from indexing.bigram_index import BigramIndex
from indexing.positional_index import PositionalIndex
from preprocess import processor, pre_process_data
from reader import get_persian_data, get_english_data


def search(query, positional_index):
    query_vector, words = ltc(query, positional_index)
    documents = get_documents(words, positional_index)
    docs = get_relevant_documents(words, query_vector, documents, positional_index)
    return docs


def proximity_search(query, positional_index, window):
    query_vector, words = ltc(query, positional_index)
    documents = get_documents_proximity(words, positional_index, window)
    docs = get_relevant_documents(words, query_vector, documents, positional_index)
    return docs


def ltc(txt, positional_index):
    txt_set = list(set(txt))
    vector = [1 + math.log(txt.count(element), 2) for element in txt_set]
    freqs = positional_index.get_freq()
    freqs_dict = {}
    for (word, freq) in freqs:
        if word in txt_set:
            freqs_dict[word] = freq
    vector = [vector[i] * math.log(1570 / freqs_dict[txt_set[i]], 2) for i in range(len(txt_set))]
    norm2 = math.sqrt(sum([element * element for element in vector]))
    vector = [element / norm2 for element in vector]
    return vector, txt_set


def lnc(doc_number, terms, positional_index):
    doc_vector = [1 + math.log(positional_index.get_frequency_of_word_in_doc(term, doc_number), 2) for term in terms]
    norm2 = math.sqrt(sum([element * element for element in doc_vector]))
    doc_vector = [element / norm2 for element in doc_vector]
    return doc_vector


def get_relevant_documents(terms, query_vector, docs, positional_index):
    relevant_docs = {}
    for docID in docs:
        doc_vector = lnc(docID, terms, positional_index)
        if len(doc_vector) == len(query_vector):
            score = sum([doc_vector[i] * query_vector[i] for i in range(len(doc_vector))])
            key, value = find_min(relevant_docs)
            if len(list(relevant_docs.keys())) > 10:
                if score > value:
                    relevant_docs.pop(key)
                    relevant_docs[docID] = score
            else:
                relevant_docs[docID] = score
        else:
            print('error in get_relevant_documents')
    return relevant_docs


def get_documents(terms, positional_index):
    documents = []
    for term in terms:
        postings = list(positional_index.get_posting_docids(term))
        for posting in postings:
            if posting not in documents:
                documents.append(posting)
    return documents


def in_range_position(postings, position, window):
    for posting in postings:
        if not (bisect_right(posting, position - window) and bisect_left(posting, position + window)):
            return False
    return True


def get_documents_proximity(terms, positional_index, window):
    documents = set([i for i in range(1580)])
    for term in terms:
        postings = set(positional_index.get_posting_docids(term))
        documents = documents.intersection(postings)
    docs = []
    for doc in documents:
        postings = po.get_postings_of_doc_for_terms(doc, terms)
        min_posting = postings[0]
        for posting in postings:
            if posting.__len__() < min_posting.__len__():
                min_posting = posting
        for position in min_posting:
            if in_range_position(postings, position, window):
                docs.append(doc)
                break

    return docs


def correct_query(query, positional_index):
    corrected_query = []
    all_words = positional_index.get_dictionary_words()
    for word in query:
        if word not in all_words:
            nearest_word = get_nearest(word)
            if nearest_word is None:
                nearest_word = '**found nothing similar to this word**'
            corrected_query.append(nearest_word)
        else:
            corrected_query.append(word)
    return corrected_query


def edit_distance(word1, word2):
    m, n = len(word1), len(word2)
    dp = [[0 for x in range(n + 1)] for x in range(m + 1)]
    for i in range(m + 1):
        for j in range(n + 1):
            if i == 0:
                dp[i][j] = j
            elif j == 0:
                dp[i][j] = i
            elif word1[i - 1] == word2[j - 1]:
                dp[i][j] = dp[i - 1][j - 1]
            else:
                dp[i][j] = 1 + min(dp[i][j - 1], dp[i - 1][j], dp[i - 1][j - 1])
    return dp[m][n]


def find_min(dictionary):
    if dictionary != {}:
        min_key, min_value = '', 20
        for key, value in dictionary.items():
            if value < min_value:
                min_key, min_value = key, value
        return min_key, min_value
    return None, None


def jaccard(word1, word2, bigram_index):
    two_grams1 = set(bigram_index.get_two_grams(str(word1)))
    two_grams2 = set(bigram_index.get_two_grams(str(word2)))
    return len(two_grams1.intersection(two_grams2)) / len(two_grams1.union(two_grams2))


def get_nearest(word):
    bigram_index = BigramIndex()
    two_grams = bigram_index.get_two_grams(word)
    distances = {}
    checked_words = []
    for two_gram in two_grams:
        words = bigram_index.get_word(two_gram)[1]
        for w in words:
            if w not in checked_words:
                checked_words.append(w)
                new_distance = jaccard(word, w, bigram_index)
                key, value = find_min(distances)
                if key is not None:
                    if len(list(distances.keys())) < 10:
                        distances[w] = new_distance
                    elif new_distance > value:
                        distances.pop(key)
                        distances[w] = new_distance
                if key is None:
                    distances[w] = new_distance
    for key, value in distances.items():
        distances[key] = edit_distance(key, word)
        key, value = find_min(distances)
    print(distances)
    if len(list(distances.keys())) == 0:
        return None
    return key


def show_instruction(state):
    if state == 0:  # start of the interface
        print('1. preprocess a new text')
        print('2. show frequent words')
        print('3. get full posting list of a word')
        print('4. get docIDs of a word')
        print('5. correct the query')
        print('6. insert a query for search')
        print('7. show file size comparisons')
        print('8. insert a query for proximity search')
        print('9. preprocess persian text')
        print('10. preprocess english text')
    if state == 1:  # invalid input was written
        print('input was invalid')


def valid_input(state):
    if state == 0:
        return ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'exit']


def execute(state, order, positional_index):
    if order == 'exit':
        return 'exit', 0
    elif state == 0 and order == '1':  # get a new text and output the preprocessed text
        print('type a new text to be tokenized: ')
        new_txt = input()
        preprocessed = processor.process(new_txt)
        print(preprocessed)
        return 'done', 0
    elif state == 0 and order == '2':  # show frequent words of all of your texts
        print('frequent words are: ')
        frequencies = positional_index.get_freq()
        frequent_words = []
        for (word, freq) in frequencies:
            if freq / 1570 > 0.4:
                frequent_words.append(word)
        print(frequent_words)
        return 'done', 0
    elif state == 0 and order == '3':  # getting a word as input and getting its full posting list as output
        print('type a new word and get the posting list: ')
        new_txt = input()
        print(positional_index.get_posting(new_txt))
        return 'done', 0
    elif state == 0 and order == '4':  # getting a word as input and getting its posting list's doc ids as output
        print('type a new word and get the doc IDs: ')
        new_txt = input()
        print(positional_index.get_posting_docids(new_txt))
        return 'done', 0
    elif state == 0 and order == '5':  # getting a query and correct the words if they are not in the dictionary
        print('insert a query: ')
        query = input()
        query = processor.process(query)
        new_query = correct_query(query, positional_index)
        print('did you mean? ', end='')
        print(new_query)
        return 'done', 0
    elif state == 0 and order == '6':  # get a query and output sorted related documents
        print('insert a query')
        query = input()
        query = processor.process(query)
        new_query = correct_query(query, positional_index)
        results = search(new_query, positional_index)
        print('results are: ', end='')
        print(results)
        return 'done', 0
    elif state == 0 and order == '7':
        vb = Compressor('vb')
        vb.compress(positional_index.pos_index)
        gamma = Compressor('gamma')
        gamma.compress(positional_index.pos_index)
        print('size of files: ', end='')
        print(get_size_of_files())
        return 'done', 0
    elif state == 0 and order == '8':  # get a query and output sorted documents with proximity search
        print('insert a query')
        query = input()
        print('insert a window num')
        window = int(input())
        query = processor.process(query)
        new_query = correct_query(query, positional_index)
        results = proximity_search(new_query, positional_index, window)
        print('results are: ', end='')
        print(results)
        return 'done', 0
    elif state == 0 and order == '9':
        pre_process_data(get_persian_data())
        return 'done', 0
    elif state == 0 and order == '10':
        pre_process_data(get_english_data())
        return 'done', 0


if __name__ == '__main__':
    po = PositionalIndex()
    po.load()

    last_state, state = 0, 0
    while True:
        last_state, state = 0, 0
        show_instruction(state)
        inp = input()
        while inp not in valid_input(state):
            last_state, state = state, 1
            show_instruction(state)
            state = last_state
            show_instruction(state)
            inp = input()
        result, state = execute(state, inp, po)
        if result == 'exit':
            break
