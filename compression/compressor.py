import copy
import os

import file_mananger
from compression.gamma import encode as g_encode
from compression.variable_byte import decode as vb_decode
from compression.variable_byte import encode_number as vb_encode


class Compressor:
    def __init__(self, type):
        self.type = type

    def compress(self, pos_index):
        index = copy.deepcopy(pos_index)
        for term, value in index.items():
            for fileno, positions in value[1].items():
                pre = 0
                for idx, item in enumerate(positions):
                    temp = item
                    positions[idx] = self.encode(item - pre)
                    pre = temp
                if self.type == 'vb':
                    value[1][fileno] = b''.join(positions)
                else:
                    value[1][fileno] = positions

        file_mananger.store(self.type, index)
        return index

    def load(self):
        index = file_mananger.load(self.type)
        for term, value in index.items():
            for fileno, positions in value[1].items():
                positions = self.decode(positions)
                pre = 0
                for idx, item in enumerate(positions):
                    positions[idx] = item + pre
                    pre = positions[idx]
                value[1][fileno] = positions
        return index

    def encode(self, data):
        if self.type == 'gamma':
            return g_encode(data)
        elif self.type == 'vb':
            return vb_encode(data)
        else:
            raise RuntimeError()

    def decode(self, data):
        if self.type == 'vb':
            return vb_decode(data)
        else:
            raise RuntimeError()


def get_size_of_files():
    return {
        'vb': file_mananger.get_size('vb'),
        'gamma': file_mananger.get_size('gamma'),
        'decompress': file_mananger.get_size('pos_index')
    }


# print(get_size_of_files())
