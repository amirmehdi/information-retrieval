def encode(n: int) -> str:
    rbc = reduced_binary_coding(n)
    str = inverted_unary(len(rbc)) + rbc
    return int(str, base=2)


def _unary(n: int, zero: str, one: str) -> str:
    return one * n + zero


def unary(n: int) -> str:
    return _unary(n, "0", "1")


def inverted_unary(n: int) -> str:
    return _unary(n, "1", "0")


def reduced_binary_coding(n: int) -> str:
    return bin(n + 1)[3:]
