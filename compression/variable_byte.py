from struct import pack, unpack


def encode_number(number):
    bytes_list = []
    while True:
        bytes_list.insert(0, number % 128)
        if number < 128:
            break
        number = number // 128
    bytes_list[-1] += 128
    return pack('%dB' % len(bytes_list), *bytes_list)


def encode_postings(postings):
    bytes_list = []
    pre_fileno = 0
    for fileno, positions in postings.items():
        bytes_list.append(encode_number(fileno - pre_fileno))
        bytes_list.append(encode(positions))
        bytes_list.append(encode_number(-1))
        pre_fileno = fileno
    return b"".join(bytes_list)


def encode(numbers):
    bytes_list = []
    for i, number in enumerate(numbers):
        if i == 0:
            bytes_list.append(encode_number(number))
        else:
            bytes_list.append(encode_number(number - numbers[i - 1]))
    return b"".join(bytes_list)


def decode(bytestream):
    n = 0
    numbers = []
    bytestream = unpack('%dB' % len(bytestream), bytestream)
    for byte in bytestream:
        if byte < 128:
            n = 128 * n + byte
        else:
            n = 128 * n + (byte - 128)
            numbers.append(n)
            n = 0
    return numbers
