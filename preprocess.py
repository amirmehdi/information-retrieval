from __future__ import unicode_literals
from hazm import *
from reader import get_persian_data
import re
from indexing.positional_index import PositionalIndex


class processor:
    def __init__(self):
        pass

    @staticmethod
    def get_stop_words():
        return stopwords_list()

    @staticmethod
    def process(txt):
        text = re.sub(r'[^\w\s]', ' ', txt)  # omits the punctuations
        normalizer = Normalizer()
        text = text.lower()
        text = normalizer.normalize(text)  # normalizing the test
        text = word_tokenize(text)  # tokenize the text
        stemmer, lemmatizer = Stemmer(), Lemmatizer()
        for idx in range(len(text)):
            text[idx] = lemmatizer.lemmatize(stemmer.stem(text[idx]))  # first stem then lemmatize the text
        stopwords = processor.get_stop_words()
        text = [word for word in text if word not in stopwords]  # delete stop words from the text
        return text

    def process_all_texts(self, texts):
        texts_dict = {}
        for idx in range(len(texts)):
            if idx % 10 == 0:
                print(idx)
            title, txt = self.process(texts[idx]['title']), self.process(texts[idx]['text'])
            texts_dict[idx] = dict({'title': title, 'text': txt})
        return texts_dict


def pre_process_data(data=get_persian_data()):
    pp = processor()
    all_texts = data
    all_texts = pp.process_all_texts(all_texts)
    po = PositionalIndex()
    for idx, terms in all_texts.items():
        po.doc_index(idx, terms['text'])
        po.doc_index(idx, terms['title'])
    po.store()

