from __future__ import unicode_literals
from hazm import *
from reader import get_persian_data
import re


def allowed_characters():
    space_codepoints = '\u0020\u2000-\u200F\u2028-\u202F'
    persian_alpha_codepoints = '\u0621-\u0628\u062A-\u063A\u0641-\u0642\u0644-\u0648\u064E-\u0651\u0655\u067E\
    u0686\u0698\u06A9\u06AF\u06BE\u06CC'
    persian_num_codepoints = '\u06F0-\u06F9'
    additional_arabic_characters_codepoints = '\u0629\u0643\u0649-\u064B\u064D\u06D5'
    arabic_numbers_codepoints = '\u0660-\u0669'
    return persian_alpha_codepoints, persian_num_codepoints


def replace_multiple(main_string, to_be_replaced, new_string):
    for ch in to_be_replaced:
        if ch in main_string:
            main_string = main_string.replace(ch, new_string)
    return main_string


def make_punctuations():
    punctuation = [':', ']', '[', '\'', '*', '=', '}', '{', '|', '&', ';', ')', '(', '/', '?', '،', '؛', '#', '%',
                   '!', '.', '.', '«', '»', '_', '<', '>', '٪', ',', '-']
    for c in range(ord('a'), ord('z') + 1):
        punctuation.append(chr(c))
    for c in range(ord('0'), ord('9') + 1):
        punctuation.append(chr(c))
    for c in range(ord('A'), ord('Z') + 1):
        punctuation.append(chr(c))
    return punctuation


def make_characters():
    chars = []
    for c in range(ord('ا'), ord('ی') + 1):
        chars.append(chr(c))
    for c in range(ord('۰'), ord('۹') + 1):
        chars.append(chr(c))
    return chars


text = get_persian_data()
punctuations = make_punctuations()
# print(punctuations)
print(text[13])
# text = replace_multiple(text, punctuations, ' ')
