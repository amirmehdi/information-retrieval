import os

try:
    import cPickle as pickle
except ImportError:  # python 3.x
    import pickle


def store(file_name, data):
    with open('generated/' + file_name + '.p', 'wb') as fp:
        pickle.dump(data, fp, protocol=pickle.HIGHEST_PROTOCOL)


def load(file_name):
    with open('generated/' + file_name + '.p', 'rb') as fp:
        return pickle.load(fp)
    return None


def get_size(file_name):
    return os.path.getsize('generated/' + file_name + '.p')
