import csv
import xml.dom.minidom


def get_persian_data():
    with open("./data/Persian.xml", encoding="utf8") as xml_file:
        dom_tree = xml.dom.minidom.parse(xml_file)
        collection = dom_tree.documentElement
        pages = collection.getElementsByTagName('page')
        page_list = []
        for t in pages:
            title = t.getElementsByTagName('title')[0].childNodes[0].data
            text = t.getElementsByTagName('revision')[0].getElementsByTagName('text')[0].childNodes[0].data
            page_list.append({'title': title, 'text': text})
    return page_list


def get_english_data():
    with open('./data/ted_talks.csv') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        page_list = []
        line_count = 0
        for row in csv_reader:
            if line_count == 0:
                line_count += 1
            else:
                page_list.append({'title': row[14], 'text': row[1]})

    return page_list
