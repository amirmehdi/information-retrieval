import file_mananger


# pos_index is a dict : pod_index[term][0] is number of occurrence, pos_index[term][1] is a dict {fileno:[positionals]}
class PositionalIndex:

    def __init__(self, pos_index=None) -> None:
        if pos_index is None:
            pos_index = {}
        self.pos_index = pos_index

    def doc_index(self, fileno, text):
        for i, term in enumerate(text):
            self.index(term, fileno, i)

    def index(self, term, fileno, pos):
        if term in self.pos_index:
            self.pos_index[term][0] = self.pos_index[term][0] + 1
            if fileno in self.pos_index[term][1]:
                self.pos_index[term][1][fileno].append(pos)
            else:
                self.pos_index[term][1][fileno] = [pos]

        else:
            self.pos_index[term] = []
            self.pos_index[term].append(1)
            self.pos_index[term].append({})
            self.pos_index[term][1][fileno] = [pos]

    def remove(self, fileno):
        deleted_term = []
        for term, value in self.pos_index.items():
            if value[1].__contains__(fileno):
                value[0] -= value[1][fileno].__len__()
                if value[0] == 0:
                    deleted_term.append(term)
                else:
                    value[1].pop(fileno)
        for term in deleted_term:
            self.pos_index.pop(term)

    def get_posting(self, term):
        return self.pos_index.get(term)

    def get_posting_docids(self, term):
        return self.pos_index.get(term)[1].keys()

    def get_dictionary_words(self):
        return self.pos_index.keys()

    def get_freq(self):
        freq = []
        for term, value in self.pos_index.items():
            freq.append((term, value[1].__len__()))
        return freq

    def store(self):
        file_mananger.store('pos_index', self.pos_index)

    def load(self):
        self.pos_index = file_mananger.load('pos_index')
        return self.pos_index

    def get_frequency_of_word_in_doc(self, word, doc_id):
        if word in self.pos_index:
            posting = self.pos_index.get(word)[1]
            if doc_id in posting:
                return posting.get(doc_id).__len__()
        return 0.5

    def get_postings_of_doc_for_terms(self, doc_id, terms):
        postings = []
        for term in terms:
            postings.append(self.pos_index.get(term)[1].get(doc_id))
        return postings


def test():
    po = PositionalIndex()
    po.doc_index(1, ['mehdi', 'salam', 'ali', 'salam'])
    po.doc_index(2, ['ali', 'salam', 'sara', 'salam'])
    po.remove(2)
    po.doc_index(3, ['farah', 'bye', 'salam'])
    print(po.get_posting('mehdi'))
    print(po.get_posting('salam'))
    print(po.get_freq())
    print(po.get_frequency_of_word_in_doc('salam', 1))
    po.store()
    print(po.load())
    from compression.compressor import Compressor
    co = Compressor('vb')
    print(co.compress(po.pos_index))
    print(co.load())
    co1 = Compressor('gamma')
    print(co1.compress(po.pos_index))

# test()
