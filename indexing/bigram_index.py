from nltk import ngrams

from indexing.positional_index import PositionalIndex


class BigramIndex:
    def __init__(self):
        self.dict_index = {}
        po = PositionalIndex()
        self.doc_index(po.load().keys())

    def doc_index(self, terms):
        for term in terms:
            self.index(term)
        return self.dict_index

    @staticmethod
    def get_two_grams(term):
        grams = list(ngrams(term, 2))
        two_grams = []
        for gram in grams:
            two_grams.append(''.join(gram))
        return two_grams

    def index(self, term):
        grams = list(ngrams(term, 2))
        two_grams = []
        for gram in grams:
            join = ''.join(gram)
            two_grams.append(join)
            self.grams_index(term, join)
        return two_grams

    def grams_index(self, term, gram):
        if gram in self.dict_index:
            self.dict_index[gram][0] += 1
            self.dict_index[gram][1].append(term)
        else:
            self.dict_index[gram] = [1, []]
            self.dict_index[gram][1].append(term)

    def get_word(self, two_gram):
        return self.dict_index.get(two_gram)


def test():
    bi = BigramIndex()
    bi.doc_index(['ali', 'mehdi', 'dirooz', 'alisnks'])
    print(bi.dict_index)
    print(bi.get_word('al'))

# test()
